import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders quotes page', () => {
  const { getByText } = render(<App />);
  const title = getByText(/All Quotes/i);
  expect(title).toBeInTheDocument();
});
