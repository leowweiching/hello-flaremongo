import React from 'react';
import styled from 'styled-components'
import { Card } from 'semantic-ui-react'
import QuoteItem from './QuoteItem'

class QuoteItemGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const Padding = styled.div`
          padding: 3em 0;
        `;
        const StyledH3 = styled.h3`
            color: white;
        `;
        const data = this.props.data;
        const listItems = data.map((item, key) => <QuoteItem item={item} key={key}/>);
        return (
          <Padding>
          { data && data.length > 0
            ? <Card.Group centered>{listItems}</Card.Group>
            : <StyledH3>No matches :(</StyledH3>
          }
          </Padding>
        )
    }
}
export default QuoteItemGroup;