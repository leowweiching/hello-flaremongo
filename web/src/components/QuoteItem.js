import React from 'react';
import { Card, Label, Icon } from 'semantic-ui-react'

class QuoteItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Card>
                <Card.Content>
                    <Card.Header>{this.props.item.source}</Card.Header>
                    <Card.Meta>{this.props.item.author}</Card.Meta>
                    <Card.Description>
                      {this.props.item.quote}
                    </Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <Label>{this.props.item.type}</Label>
                    <Label icon='globe' content={this.props.item.language} />
                </Card.Content>
            </Card>
        )
    }
}
export default QuoteItem;