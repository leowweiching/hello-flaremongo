import React from 'react';
import Page from './Page'
import QuoteItemGroup from './QuoteItemGroup'

class QuotePage extends React.Component {
    constructor(props) {
        super(props);
        this.handler = this.handler.bind(this)
        this.state = {
            data: [],
            key: "all",
            query: ""
        };
    }

    componentDidMount() {
        this.fetchData("/api/all")
    }

    handler = (key, query) => {
        this.setState({ ...this.state, key: key, query: query })
        const url = (query == "") ? "/api/all" : "/api/" + key + "/" + query
        this.fetchData(url)
    }

    fetchData = (url) => {
        fetch(url)
            .then(response => {
                return response.json()
            })
            .then(data => this.setState({ ...this.state, data: data }));
    } 

    render() {
        const QuoteComponent = <QuoteItemGroup data={this.state.data} />;
        const title = (this.state.query == "") ? "All Quotes 💬" : "Searched \"" + this.state.query.trim() + "\" in " + this.state.key.trim();
        return <Page title={title} body={QuoteComponent} handler={this.handler}></Page>
    }
}
export default QuotePage;