import React from 'react';
import styled from 'styled-components'
import { Button, Select, Input, List } from 'semantic-ui-react'

const options = [
  { key: 'all', text: 'All Fields', value: 'all' },
  { key: 'type', text: 'Type', value: 'type' },
  { key: 'language', text: 'Language', value: 'language' },
  { key: 'source', text: 'Title', value: 'source' },
  { key: 'quote', text: 'Quote', value: 'quote' },
  { key: 'author', text: 'Author', value: 'author' }
]

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
  		    key: "all",
  		    query: ""
		    }
    }

    handleInputChange = (e) => {
    	this.setState({ ...this.state, query: e.target.value })
    }

    handleDropdownChange = (e, { value }) => {
      this.setState({ ...this.state, key: value })
    }

    render() {
        const StyledList = styled(List)`
          color: white;
        `;
        return (
          <React.Fragment>
            <Input fluid type='text' placeholder='Search...' action>
            	<input onChange={this.handleInputChange} onKeyPress={event => {
                if (event.key === 'Enter') {
                  this.props.handler(this.state.key, this.state.query)
                }
              }} />
	    		<Select options={options} onChange={this.handleDropdownChange} defaultValue='all' />
	    		<Button color='black' type='submit' onClick={() => this.props.handler(this.state.key, this.state.query)} >Search</Button>
 			</Input>
      <StyledList>
        <List.Item>Searching in "All Fields" does full word-matching</List.Item>
        <List.Item>Searching by specific field (e.g. type, author) does partial/full word-matching</List.Item>
      </StyledList>
      </React.Fragment>
        )
    }
}

export default Search