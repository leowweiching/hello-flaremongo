import React from 'react';
import styled from 'styled-components'

import Search from './Search'

class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

    	const PaddedPage = styled.div`
	  		padding: 3em;
            height: 100vh;
            overflow: scroll;
            background: #FF512F;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #DD2476, #FF512F);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #DD2476, #FF512F); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
		`;

        const StyledH1 = styled.h1`
            color: white;
            padding: 1em;
        `;

    	return (
            <PaddedPage>
                <StyledH1>{this.props.title}</StyledH1>
                <Search handler={this.props.handler} />
                {this.props.body}
            </PaddedPage>)
    }
}
export default Page;