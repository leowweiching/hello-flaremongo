from bson.json_util import dumps
from pymongo import MongoClient

client = MongoClient("mongodb://quotes-db-service:27017")
db = client.quotes_db

db.quotes.create_index([("type", "text"), ("language", "text"),
                        ("source", "text"), ("quote", "text"), ("author", "text")], language_override="en")


# Returns all items
def get_all_quotes():
    return dumps(list(db.quotes.find({})))


# Searches across fields, returns items where whole word is matched
def search_all_quotes(query):
    return dumps(list(db.quotes.find({"$text": {"$search": query}})))


# Searches `key` field only, returns items where `query` partially/fully matches field value
def get_by(key, query):
    return dumps(list(db.quotes.find({key: {"$regex": query, "$options": "$i"}})))
