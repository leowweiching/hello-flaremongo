import db

from flask import Flask

app = Flask(__name__)


@app.route("/test")
def test():
    return "test 888 string"


@app.route("/all")
def get_all_quotes():
    return db.get_all_quotes()


@app.route("/all/<query>")
def search_all_quotes(query):
    return db.search_all_quotes(query)


@app.route("/type/<type>")
def get_by_type(type):
    return db.get_by("type", type)


@app.route("/language/<language>")
def get_by_language(language):
    return db.get_by("language", language)


@app.route("/author/<author>")
def get_by_author(author):
    return db.get_by("author", author)


@app.route("/source/<source>")
def get_by_source(source):
    return db.get_by("source", source)


@app.route("/quote/<quote>")
def get_by_quote(quote):
    return db.get_by("quote", quote)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
