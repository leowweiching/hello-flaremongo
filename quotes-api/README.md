
# hello-flaremongo - API Server

This Flask application queries the MongoDB database.

## API Endpoints

- `/api/all` : Returns a list of all quotes in database
- `/api/all/<query>`
	- Returns a list of quotes where query matches the value of any field (i.e. type, language, source, quote) 
	- Matches whole words
-  `/api/type/<query>`
	- Returns a list of quotes where query matches the value of `type`
	- Matches partial or whole words
-  `/api/language/<query>`
	- Returns a list of quotes where query matches the value of `language`
	- Matches partial or whole words
-  `/api/source/<query>`
	- Returns a list of quotes where query matches the value of `source`
	- Matches partial or whole words
-  `/api/quote/<query>`
	- Returns a list of quotes where query matches the value of `quote`
	- Matches partial or whole words