# hello-flaremongo

hello-flaremongo is a Flask + React + MongoDB project that I wrote to learn how to write `Dockerfile` and `docker-compose.yml` files and practise linking up Docker containers.

- MongoDB stores a bunch of tv and movie quotes (data obtained from https://github.com/msramalho/json-tv-quotes/blob/master/quotes.json)
- Flask API server interacts with the database
- React front-end queries the API server

## Usage

### Development

```bash
docker-compose up -d
```

In your browser, go to `localhost:4000`.


## Screenshot

![React Front-End](./QuotesDB.png)